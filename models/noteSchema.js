var mongoose = require('mongoose');
var Schema = mongoose.Schema;

mongoose.connect('mongodb://localhost/guestbook');

function lengthValidator(val){
    return val.length <= 255;   
}

var custom = [lengthValidator, 'greater than 255!'];

var noteSchema = new Schema({
    title: { type: String, required:'title is required!'},
    author: { type: String, required:'author is required!'},
    message: {type: String, validate: custom }
});

var Notes = mongoose.model('Notes', noteSchema);

module.exports = {
    notes: Notes
}