var express = require('express');
var multer = require('multer');
var router = express.Router();
var schema = require('../models/noteSchema');
var fs = require('fs');
var done = false;

//  set Model for note crud operations
var Notes = schema.notes;

// everytime request is made to api
router.use(function(req, res, next){
    console.log('something is happening');
    next();
});


router.use('/notes', multer({
    dest: './uploads/',
    limits: {
        fileSize: 15000000  
    },
    rename: function(fieldname, filename){
        return filename+Date.now();
    },
    onFileUploadStart: function(file){
        console.log(file.originalname + ' is starting ...');   
    },
    onFileUploadComplete: function(file){
        console.log(file.fieldname + ' uploaded to ' + file.path);
        console.log(file);
        done = true;
    },
    onFileSizeLimit: function (file){
        console.log('failed: ', file.originalname);
        fs.unlink('./' + file.path);
    }
}));


router.get('/notes', function(req, res){
    Notes.find(function(err, notes){
       if(err) return console.error(err);
        res.render('notes', {notes: notes});
    });
}).post('/notes', function(req, res, next){
    
        var postBody = req.body;
    
        // check to see if posting for Deletion
        for(key in postBody){
            if(key === 'id'){
                Notes.findByIdAndRemove(postBody.id, function(err, removedNote){
                    if(err)
                        res.send(err);
                    Notes.find(function(err, notes){
                       if(err) return console.error(err);
                        res.render('notes', {notes: notes});
                    });
                });
                return;
            }
        }
        
        // create and save note
        var note = new Notes(req.body);
        //console.log(note.id);
    
        console.log('posted');
    
        note.save(function (err, newNote) {
            if (err) { console.error(err); }
            // re-route back to home page
            Notes.find(function(err, notes){
                if(err) return console.error(err);
                res.render('notes', {notes: notes});
            });
        });  
});

/* Individual Notes */
router.route('/notes/:note_id')
    .get(function(req, res) {
        Notes.findById(req.params.note_id, function(err, note) {
            if (err)
                res.send(err);
            res.render('note', {note: note});
        });
    })
    .patch(function(req, res){
        Notes.findByIdAndUpdate(req.params.note_id, req.body, function(err, updatedNote){
            console.log(updatedNote);
            if(err)
                res.send(err);
            res.json(updatedNote);
        });
    })
    .delete(function(req, res){
        Notes.findByIdAndRemove(req.params.note_id, function(err, removedNote){
            console.log(removedNote);
            if(err)
                res.send(err);
            res.json(removedNote);
        });
    });

module.exports = router;